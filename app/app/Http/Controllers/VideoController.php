<?php

namespace App\Http\Controllers;

use App\Videos;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    public function showVideo()
    {
        $video = Videos::all();
        return response()->json($video);
    }

    public function create(Request $request)
    {
                  
         $this->validate($request, [
            'url' => 'required',
            'title' => 'required',
            'descricao' => 'required',
            'autor' => 'required'
            
        ]);

        $video = Videos::create($request->all());

        return response()->json($video, 201);
      
      
        
    }

    public function update($id, Request $request)
    {

        $this->validate($request, [
            
            'title' => 'required',
            'descricao' => 'required'
                        
        ]);

        $video= Videos::find($id);
        
        $video->title = $request->input('title');
        $video->descricao = $request->input('descricao');
      
        $video->save();
        return response()->json($video);
    }

    public function delete($id)
    {
        Videos::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    
}